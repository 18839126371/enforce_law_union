from django.apps import AppConfig


class EnforceLawConfig(AppConfig):
    name = 'enforce_law'
