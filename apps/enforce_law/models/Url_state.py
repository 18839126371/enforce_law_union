from django.db import models

# Create your models here.


class Url_state(models.Model):
    '''
    url状态记录表。
    '''
    path = models.CharField(max_length=50, verbose_name='url路径')
    incident = models.CharField(max_length=50, verbose_name='事件（自定义）', null=True, blank=True)
    state = models.CharField(max_length=2, choices=[('1', "正常"), ('2', "瘫痪中")], verbose_name='url状态', default='1')
    msg = models.CharField(max_length=60, verbose_name='url状态', default='该服务正在维护，暂停使用。请各位用户谅解，对您造成的不便，我们深表歉意。')

    class Meta:
        verbose_name = "程序url状态"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.path