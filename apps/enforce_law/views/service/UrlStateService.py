from comk_django_plugin.utils.PublicServer import PublicServer
from django.http import HttpRequest
from enforce_law.views.dao.UrlStateDao import UrlStateDao
from django.db.models import Q

class UrlStateService(PublicServer):
    def __init__(self, request: HttpRequest):
        super().__init__(request)

    def do_method_check(self, method):
        '''
        检查方法是否维护

        :param method:
        :return:
        '''
        path = self.request.path
        us = UrlStateDao().get_data({'path': path, 'state': '2'})

        us_all = us.filter(Q(incident__isnull=True) | Q(incident__in=['']))
        if us_all:
            return self.build_error_response_data(code='1001', msg=us_all[0].msg)

        us_method = us.filter(incident=method)
        if us_method:
            return self.build_error_response_data(code='1001', msg=us_method[0].msg)

