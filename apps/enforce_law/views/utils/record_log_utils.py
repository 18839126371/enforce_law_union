import logging


def record_program_exception(msg):
    log = logging.getLogger('program_exception')
    log.exception(msg)


def record_database_exception(msg):
    log = logging.getLogger('database_exception')
    log.exception(msg)


def record_other_log(msg):
    log = logging.getLogger('other_log')
    log.exception(msg)