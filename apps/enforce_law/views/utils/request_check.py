# -*- coding: utf-8 -*-

import json
import logging


from comk_django_plugin import PublicServer, general_resolve_request

from enforce_law.views.conf.function import FUNCTIONS


def request_to_response(function):
    """
    用户访问权限校验，以及业务正常失败处理

    :param func:
    :return:
    """

    def wrap(*args, **kwargs):
        request = args[0]  # 获取request
        ps = PublicServer(request)
        resp = ps.response_data
        # if not ps.check_login_user():
        #     ps.login_user(username='jack', password='admin123')
        method = request.method
        if method != 'POST':
            resp['code'] = '2002'
            resp['msg'] = 'this server is only support POST(method),please try again'
            return ps.return_json_response(resp)

        body = request.body
        if not body:
            resp['code'] = '2005'
            resp['msg'] = 'data error,data is null'
            return ps.return_json_response(resp)
        try:
            data = json.loads(body)
        except json.decoder.JSONDecodeError:
            resp['code'] = '2006'
            resp['msg'] = 'data error,json-str is needed'
            return ps.return_json_response(resp)

        method = data.get('method')

        if method not in FUNCTIONS.keys():
            resp['code'] = '4005'
            resp['msg'] = 'the method(%s) is not support now' % (method)
            return ps.return_json_response(resp)

        try:
            result = function(*args, **kwargs)
            return ps.return_json_response(result)

        except Exception as e:
            resp['code'] = '9999'
            resp['msg'] = 'system error'
            do_Exception_log(request)
            return ps.return_json_response(resp)

    return wrap


def do_Exception_log(request):
    try:
        request_result = general_resolve_request(request)
        log = logging.getLogger('comk_exception_log')  # 加载记录器
        log.exception(request_result)
    except:
        pass
