from django.db import models
from enforce_law.models import Url_state
from enforce_law.views.dao.PublicDao import PublicDao


class UrlStateDao(PublicDao):
    def __init__(self, model: models.Model = Url_state):
        super().__init__(model)

