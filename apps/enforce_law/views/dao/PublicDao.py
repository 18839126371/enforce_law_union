from django.db import models
from comk_django_plugin.utils.PublicDao import PublicDao as pub


class PublicDao(pub):

    def get_data(self, keys=None):
        manager=self.model.objects
        if keys:
            filter_data = manager.filter(**keys)
        else:
            filter_data = manager.all()
        return filter_data

    def get_model_fields(self):
        '''获取model中的字段'''
        model_fields = []
        fields = self.model._meta.get_fields()
        for field in fields:
            model_fields.append(field.name)
        return model_fields

    def filter_data_by_model_fields(self, data):
        """在data中找出model中的字段"""
        keys = dict()
        model_fields = self.get_model_fields()
        for k, v in data.items():
            if k in model_fields and k != 'id':
                keys.update({k: v})
        return keys