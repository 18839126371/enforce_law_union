from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View
from django.shortcuts import render


# @method_decorator(is_login, name='get')
@method_decorator(ensure_csrf_cookie, name='get')
class Vue(View):
    '''
    渲染入口
    '''

    def get(self, request):
        return render(request, 'index.html')