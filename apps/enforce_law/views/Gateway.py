import json
from django.utils.decorators import method_decorator
from django.views import View
from enforce_law.views.conf.function import FUNCTIONS
from enforce_law.views.utils.request_check import request_to_response
from enforce_law.views.service.UrlStateService import UrlStateService


import logging
log_ex=logging.getLogger('traffic_exception')


@method_decorator(request_to_response, name='post')
class Gateway(View):
    """
        功能集中调用

    """

    def post(self, request):
        resp = {'code': '8999','response_data': '', 'msg': ''}
        try:
            data = json.loads(request.body)
            method = data.get('method', '')
        except :
            method=request.POST.get('method','')
        try:
            if method == 'test':
                return {'success': True}
            if method in FUNCTIONS.keys():
                    method_status = self.check_method_status(request, method)  # 做开关
                    if method_status:
                        return method_status
            resp = FUNCTIONS[method](request)()

        except Exception as e:
            log_ex.exception(e)
            resp['code'] = '9999'
            resp['msg'] = '服务器提了一个问题，程序员正在解答。'
        return resp

    def check_method_status(self, request, method):
        '''
        检查方法是否维护

        :param request:
        :param method:
        :return:
        '''
        ps = UrlStateService(request)
        return ps.do_method_check(method)