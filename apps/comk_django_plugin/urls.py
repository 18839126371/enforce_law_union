from django.conf.urls import url

from comk_django_plugin.views.LogView import GetLog
from comk_django_plugin.views.GetAllLog import GetAllLog

urlpatterns = [
    url(r'^getlog/(.+)/', GetLog.as_view(), name='获取日志'),
    url(r'^get_all_log/$', GetAllLog.as_view(), name='获取全部日志'),
]
