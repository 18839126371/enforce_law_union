from django.apps import AppConfig


class ComkDjangoPluginConfig(AppConfig):
    name = 'comk_django_plugin'
    verbose_name = 'comk个人开发包'