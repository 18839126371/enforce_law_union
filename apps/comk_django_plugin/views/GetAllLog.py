from django.views import View
from comk_django_plugin import PublicServer
from django.shortcuts import render
from django.conf import settings
import os

class GetAllLog(View):
    '''
    获取全部日志
    '''
    def get(self, request):
        ps = PublicServer(request)
        check_result = ps.check_login_user()
        if not check_result:
            return ps.return_build_error_response(msg='未登录，请先通过django-admin进行登录')

        user = request.user
        if not user.is_superuser:
            return ps.return_build_error_response(msg='您不是超级用户，无法下载文件')
        BASE_DIR = settings.BASE_DIR
        file_list = os.listdir(BASE_DIR + '/log/')
        error_list = []
        request_list = []
        other_list = []
        for file in file_list:
            url ='/mfxx' + '/getlog/' + file + '/'
            if 'error' in file:
                error_list.append(url)
            elif 'request' in file:
                request_list.append(url)
            else:
                other_list.append(url)

        return render(request, 'log.html', {'errors': sorted(error_list),
'requests': sorted(request_list), 'others': sorted(other_list)})
