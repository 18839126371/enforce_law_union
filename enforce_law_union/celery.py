from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'enforce_law_union.settings')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app = Celery('enforce_law_union')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
app.conf.broker_transport_options = {'visibility_timeout':3600}    #可见性超时时间

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

app.conf.update(
    CELERYBEAT_SCHEDULE={
        # 'create_monthly_exams':
        #     {
        #         "task": "",  # task name
        #         "schedule": crontab(minute=1, hour=1, day_of_week= 1),
        #         # "schedule": crontab(minute='*/1'),
        #         "args": (),
        #     },
    }
)