"""enforce_law_union URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from enforce_law.views.Gateway import Gateway
from enforce_law.views.Vue import Vue

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^gateway/', Gateway.as_view(),name='功能接口'),
    url(r'^vue/', Vue.as_view(), name='渲染入口'),  # 渲染入口
]
