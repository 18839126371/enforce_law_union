from django.conf import settings
#
BASE_DIR = settings.BASE_DIR
import datetime

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'comk_verbose': {
            'format': '%(levelname)s %(asctime)s %(pathname)s %(message)s'
        },
    },
    'handlers': {
        'program_exception': {
            'level': 'DEBUG',  # 打印DEBUG （或更高）级别的消息。
            'class': 'logging.handlers.RotatingFileHandler',  # 它的主体程序是RotatingFileHandler类，这是最重要的。
            'filename': BASE_DIR + '/log/program_exception_' + datetime.datetime.now().strftime("%Y%m%d") + '.log',
            'maxBytes': 1024 * 1024 * 100,  # 每个日志文件大小
            'backupCount': 2,
            'formatter': 'comk_verbose',  # 采用verbose为格式化器。
            'encoding': 'utf-8',  # 设定编码，如不设定，则编码为ascii，无法写入中文
        },
        'database_exception': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': BASE_DIR + '/log/database_exception_' + datetime.datetime.now().strftime("%Y%m%d") + '.log',
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 2,
            'formatter': 'comk_verbose',
            'encoding': 'utf-8',
        },
        'other_log': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': BASE_DIR + '/log/other_log_' + datetime.datetime.now().strftime("%Y%m%d") + '.log',
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 2,
            'formatter': 'comk_verbose',
            'encoding': 'utf-8',
        },

    },
    'loggers': {
        'program_exception':{
            'handlers': ['program_exception'],
            'level': 'INFO',
        },
        'database_exception':{
            'handlers': ['database_exception'],
            'level': 'INFO',
        },
        'other_log': {
            'handlers': ['other_log'],
            'level': 'INFO',
        },
    }
}
